import java.util.*;

public class Q2 {

    static class Graph {
        private int V;
        private List<List<Integer>> adjList;

        Graph(int V) {
            this.V = V;
            adjList = new ArrayList<>(V);
            for (int i = 0; i < V; i++) {
                adjList.add(new ArrayList<>());
            }
        }

        void addEdge(int src, int dest) {
            adjList.get(src).add(dest);
            adjList.get(dest).add(src); // Undirected graph
        }

        void graphColoring() {
            int[] result = new int[V];
            Arrays.fill(result, -1);

            boolean[] available = new boolean[V];
            Arrays.fill(available, true);

            result[0] = 0;

            for (int u = 1; u < V; u++) {
                for (int neighbor : adjList.get(u)) {
                    if (result[neighbor] != -1) {
                        available[result[neighbor]] = false;
                    }
                }

                int cr;
                for (cr = 0; cr < V; cr++) {
                    if (available[cr]) {
                        break;
                    }
                }

                result[u] = cr;

                Arrays.fill(available, true);
            }

            System.out.println("Vertex\tColor");
            for (int i = 0; i < V; i++) {
                System.out.println(i + "\t" + result[i]);
            }
        }
    }

    public static void main(String[] args) {
        int V = 5; // Number of vertices
        Graph graph = new Graph(V);
        graph.addEdge(0, 1);
        graph.addEdge(0, 2);
        graph.addEdge(1, 2);
        graph.addEdge(1, 3);
        graph.addEdge(2, 3);
        graph.addEdge(3, 4);

        System.out.println("Coloring of the graph:");
        graph.graphColoring();
    }
}
