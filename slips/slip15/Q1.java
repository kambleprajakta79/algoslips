import java.util.*;

public class Q1 {

    static class Item implements Comparable<Item> {
        int weight;
        int value;
        double ratio;

        Item(int weight, int value) {
            this.weight = weight;
            this.value = value;
            this.ratio = (double) value / weight;
        }

        @Override
        public int compareTo(Item other) {
            return Double.compare(other.ratio, this.ratio);
        }
    }

    static class Node {
        int level;
        int profit;
        int weight;
        double bound;

        Node(int level, int profit, int weight) {
            this.level = level;
            this.profit = profit;
            this.weight = weight;
            this.bound = 0.0;
        }
    }

    static double bound(Node u, int n, int W, Item[] items) {
        if (u.weight >= W) {
            return 0;
        }

        double profitBound = u.profit;
        int j = u.level + 1;
        int totalWeight = u.weight;

        while (j < n && totalWeight + items[j].weight <= W) {
            profitBound += items[j].value;
            totalWeight += items[j].weight;
            j++;
        }

        if (j < n) {
            profitBound += (W - totalWeight) * items[j].ratio;
        }

        return profitBound;
    }

    static int knapsack(int W, int[] wt, int[] val, int n) {
        Item[] items = new Item[n];
        for (int i = 0; i < n; i++) {
            items[i] = new Item(wt[i], val[i]);
        }
        Arrays.sort(items);

        Queue<Node> queue = new LinkedList<>();
        Node u, v;
        u = new Node(-1, 0, 0);
        queue.add(u);

        int maxProfit = 0;
        while (!queue.isEmpty()) {
            u = queue.poll();
            if (u.level == -1) {
                v = new Node(0, 0, 0);
            } else if (u.level == n - 1) {
                continue;
            } else {
                v = new Node(u.level + 1, u.profit + val[u.level + 1], u.weight + wt[u.level + 1]);
            }

            if (v.weight <= W && v.profit > maxProfit) {
                maxProfit = v.profit;
            }

            v.bound = bound(v, n, W, items);
            if (v.bound > maxProfit) {
                queue.add(v);
            }

            v = new Node(u.level + 1, u.profit, u.weight);
            v.bound = bound(v, n, W, items);
            if (v.bound > maxProfit) {
                queue.add(v);
            }
        }

        return maxProfit;
    }

    public static void main(String[] args) {
        int[] val = {60, 100, 120};
        int[] wt = {10, 20, 30};
        int W = 50;
        int n = val.length;
        System.out.println("Maximum value that can be obtained: " + knapsack(W, wt, val, n));
    }
}
