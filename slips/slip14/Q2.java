import java.util.*;

public class Q2 {

    static class Graph {
        private int V;
        private List<List<Integer>> adjList;

        Graph(int V) {
            this.V = V;
            adjList = new ArrayList<>(V);
            for (int i = 0; i < V; i++) {
                adjList.add(new ArrayList<>());
            }
        }

        void addEdge(int src, int dest) {
            adjList.get(src).add(dest);
        }

        void DFS(int start) {
            boolean[] visited = new boolean[V];
            DFSUtil(start, visited);
        }

        void DFSUtil(int v, boolean[] visited) {
            visited[v] = true;
            System.out.print(v + " ");

            for (int neighbor : adjList.get(v)) {
                if (!visited[neighbor]) {
                    DFSUtil(neighbor, visited);
                }
            }
        }

        void BFS(int start) {
            boolean[] visited = new boolean[V];
            Queue<Integer> queue = new LinkedList<>();

            visited[start] = true;
            queue.offer(start);

            while (!queue.isEmpty()) {
                int u = queue.poll();
                System.out.print(u + " ");

                for (int neighbor : adjList.get(u)) {
                    if (!visited[neighbor]) {
                        visited[neighbor] = true;
                        queue.offer(neighbor);
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        int V = 6; // Number of vertices
        Graph graph = new Graph(V);
        graph.addEdge(0, 1);
        graph.addEdge(0, 2);
        graph.addEdge(1, 3);
        graph.addEdge(1, 4);
        graph.addEdge(2, 4);
        graph.addEdge(3, 4);
        graph.addEdge(3, 5);
        graph.addEdge(4, 5);

        System.out.println("DFS traversal starting from vertex 0:");
        long startTimeDFS = System.nanoTime();
        graph.DFS(0);
        long endTimeDFS = System.nanoTime();
        System.out.println("\nTime taken for DFS: " + (endTimeDFS - startTimeDFS) + " nanoseconds");

        System.out.println("\nBFS traversal starting from vertex 0:");
        long startTimeBFS = System.nanoTime();
        graph.BFS(0);
        long endTimeBFS = System.nanoTime();
        System.out.println("\nTime taken for BFS: " + (endTimeBFS - startTimeBFS) + " nanoseconds");
    }
}
