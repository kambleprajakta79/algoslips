import java.util.PriorityQueue;

class Q2 {

    static class HuffmanNode implements Comparable<HuffmanNode> {
        char data;
        int frequency;
        HuffmanNode left, right;

        HuffmanNode(char data, int frequency) {
            this.data = data;
            this.frequency = frequency;
        }

        @Override
        public int compareTo(HuffmanNode other) {
            return this.frequency - other.frequency;
        }
    }

    static void printCodes(HuffmanNode root, String code) {
        if (root == null) {
            return;
        }

        if (root.left == null && root.right == null && Character.isLetter(root.data)) {
            System.out.println(root.data + " : " + code);
        }

        printCodes(root.left, code + "0");
        printCodes(root.right, code + "1");
    }

    static void huffmanCodes(char[] data, int[] frequency) {
        int n = data.length;

        PriorityQueue<HuffmanNode> pq = new PriorityQueue<>();
        for (int i = 0; i < n; i++) {
            pq.offer(new HuffmanNode(data[i], frequency[i]));
        }

        while (pq.size() > 1) {
            HuffmanNode left = pq.poll();
            HuffmanNode right = pq.poll();

            HuffmanNode newNode = new HuffmanNode('$', left.frequency + right.frequency);
            newNode.left = left;
            newNode.right = right;

            pq.offer(newNode);
        }

        HuffmanNode root = pq.poll();
        System.out.println("Huffman Codes:");
        printCodes(root, "");
    }

    public static void main(String[] args) {
        char[] data = {'a', 'b', 'c', 'd', 'e', 'f'};
        int[] frequency = {5, 9, 12, 13, 16, 45};

        huffmanCodes(data, frequency);
    }
}
