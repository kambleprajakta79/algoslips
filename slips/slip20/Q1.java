import java.util.*;

public class Q1 {

    static class Graph {
        private int V;
        private List<List<Integer>> adjList;

        Graph(int V) {
            this.V = V;
            adjList = new ArrayList<>(V);
            for (int i = 0; i < V; i++) {
                adjList.add(new ArrayList<>());
            }
        }

        void addEdge(int src, int dest) {
            adjList.get(src).add(dest);
        }

        Stack<Integer> topologicalSort() {
            boolean[] visited = new boolean[V];
            Stack<Integer> stack = new Stack<>();

            for (int i = 0; i < V; i++) {
                if (!visited[i]) {
                    topologicalSortUtil(i, visited, stack);
                }
            }

            return stack;
        }

        void topologicalSortUtil(int v, boolean[] visited, Stack<Integer> stack) {
            visited[v] = true;

            for (int neighbor : adjList.get(v)) {
                if (!visited[neighbor]) {
                    topologicalSortUtil(neighbor, visited, stack);
                }
            }

            stack.push(v);
        }
    }

    public static void main(String[] args) {
        int V = 6; // Number of vertices
        Graph graph = new Graph(V);
        graph.addEdge(5, 2);
        graph.addEdge(5, 0);
        graph.addEdge(4, 0);
        graph.addEdge(4, 1);
        graph.addEdge(2, 3);
        graph.addEdge(3, 1);

        long startTime = System.nanoTime();
        Stack<Integer> result = graph.topologicalSort();
        long endTime = System.nanoTime();

        System.out.println("Topological sorting order:");
        while (!result.isEmpty()) {
            System.out.print(result.pop() + " ");
        }
        System.out.println();

        System.out.println("Time taken for topological sorting: " + (endTime - startTime) + " nanoseconds");
    }
}
