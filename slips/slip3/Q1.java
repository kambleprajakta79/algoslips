import java.util.Arrays;

public class Q1 {
	public static void selectionSort(int[] arr) {
		int n = arr.length;
		for (int i = 0; i < n - 1; i++) {
			int minIndex = i;
			for (int j = i + 1; j < n; j++) {
				if (arr[j] < arr[minIndex]) {
					minIndex = j;
				}
			}
			int temp = arr[minIndex];
			arr[minIndex] = arr[i];
			arr[i] = temp;
		}
	}

	public static void main(String[] args) {
		int[] arr = { 64, 34, 25, 12, 22, 11, 90 };

		System.out.println("Original Array: " + Arrays.toString(arr));

		long startTime = System.nanoTime();
		selectionSort(arr);
		long endTime = System.nanoTime();

		System.out.println("Sorted Array: " + Arrays.toString(arr));
		System.out.println("Time taken for sorting: " + (endTime - startTime) + " ns");
	}
}
