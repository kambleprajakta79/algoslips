import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Q2 {

    static class Edge {
        int src, dest, weight;

        Edge(int src, int dest, int weight) {
            this.src = src;
            this.dest = dest;
            this.weight = weight;
        }
    }

    static class Graph {
        int V;
        List<List<Edge>> adjList;

        Graph(int V) {
            this.V = V;
            adjList = new ArrayList<>();
            for (int i = 0; i < V; i++) {
                adjList.add(new ArrayList<>());
            }
        }

        void addEdge(int src, int dest, int weight) {
            Edge edge = new Edge(src, dest, weight);
            adjList.get(src).add(edge);
            edge = new Edge(dest, src, weight); // Since the graph is undirected
            adjList.get(dest).add(edge);
        }

        void primMST() {
            boolean[] visited = new boolean[V];
            int[] parent = new int[V];
            int[] key = new int[V];
            Arrays.fill(key, Integer.MAX_VALUE);
            key[0] = 0;
            parent[0] = -1;

            for (int i = 0; i < V - 1; i++) {
                int u = minKey(key, visited);
                visited[u] = true;

                for (Edge edge : adjList.get(u)) {
                    int v = edge.dest;
                    int weight = edge.weight;
                    if (!visited[v] && weight < key[v]) {
                        parent[v] = u;
                        key[v] = weight;
                    }
                }
            }

            printMST(parent);
        }

        int minKey(int[] key, boolean[] visited) {
            int min = Integer.MAX_VALUE;
            int minIndex = -1;

            for (int v = 0; v < V; v++) {
                if (!visited[v] && key[v] < min) {
                    min = key[v];
                    minIndex = v;
                }
            }
            return minIndex;
        }

        void printMST(int[] parent) {
            System.out.println("Edge   Weight");
            for (int i = 1; i < V; i++) {
                System.out.println(parent[i] + " - " + i + "    " + adjList.get(i).get(parent[i]).weight);
            }
        }
    }

    public static void main(String[] args) {
        int V = 5; // Number of vertices
        Graph graph = new Graph(V);

        // Adding edges to the graph
        graph.addEdge(0, 1, 2);
        graph.addEdge(0, 3, 6);
        graph.addEdge(1, 2, 3);
        graph.addEdge(1, 3, 8);
        graph.addEdge(1, 4, 5);
        graph.addEdge(2, 4, 7);
        graph.addEdge(3, 4, 9);

        // Finding MST using Prim's algorithm
        System.out.println("Minimum Spanning Tree using Prim's Algorithm:");
        graph.primMST();
    }
}
