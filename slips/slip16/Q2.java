import java.util.*;

public class Q2 {

    static class Graph {
        private int V;
        private List<List<Integer>> adjList;

        Graph(int V) {
            this.V = V;
            adjList = new ArrayList<>(V);
            for (int i = 0; i < V; i++) {
                adjList.add(new ArrayList<>());
            }
        }

        void addEdge(int src, int dest) {
            adjList.get(src).add(dest);
            adjList.get(dest).add(src); // Undirected graph
        }

        boolean isHamiltonianCycle() {
            boolean[] visited = new boolean[V];
            Arrays.fill(visited, false);

            visited[0] = true;
            if (hamCycleUtil(visited, 0, -1) && allVisited(visited)) {
                return true;
            }

            return false;
        }

        boolean hamCycleUtil(boolean[] visited, int v, int parent) {
            for (int neighbor : adjList.get(v)) {
                if (!visited[neighbor]) {
                    visited[neighbor] = true;
                    if (hamCycleUtil(visited, neighbor, v)) {
                        return true;
                    }
                    visited[neighbor] = false;
                } else if (neighbor != parent) {
                    return false;
                }
            }
            return true;
        }

        boolean allVisited(boolean[] visited) {
            for (boolean v : visited) {
                if (!v) {
                    return false;
                }
            }
            return true;
        }
    }

    public static void main(String[] args) {
        int V = 5; // Number of vertices
        Graph graph = new Graph(V);
        graph.addEdge(0, 1);
        graph.addEdge(0, 3);
        graph.addEdge(1, 2);
        graph.addEdge(1, 4);
        graph.addEdge(2, 4);
        graph.addEdge(3, 4);

        if (graph.isHamiltonianCycle()) {
            System.out.println("The graph contains a Hamiltonian cycle.");
        } else {
            System.out.println("The graph does not contain a Hamiltonian cycle.");
        }
    }


}

