import java.util.*;

public class Q2 {

    static class Graph {
        private int V;
        private List<List<Integer>> adjList;

        Graph(int V) {
            this.V = V;
            adjList = new ArrayList<>(V);
            for (int i = 0; i < V; i++) {
                adjList.add(new ArrayList<>());
            }
        }

        void addEdge(int src, int dest) {
            adjList.get(src).add(dest);
        }

        List<Integer> findLiveNodes() {
            List<Integer> liveNodes = new ArrayList<>();
            for (int i = 0; i < V; i++) {
                if (!adjList.get(i).isEmpty()) {
                    liveNodes.add(i);
                }
            }
            return liveNodes;
        }

        List<Integer> findENodes() {
            List<Integer> eNodes = new ArrayList<>();
            for (int i = 0; i < V; i++) {
                if (!adjList.get(i).isEmpty() && hasIncomingEdge(i)) {
                    eNodes.add(i);
                }
            }
            return eNodes;
        }

        List<Integer> findDeadNodes() {
            List<Integer> deadNodes = new ArrayList<>();
            for (int i = 0; i < V; i++) {
                if (adjList.get(i).isEmpty()) {
                    deadNodes.add(i);
                }
            }
            return deadNodes;
        }

        boolean hasIncomingEdge(int node) {
            for (List<Integer> neighbors : adjList) {
                if (neighbors.contains(node)) {
                    return true;
                }
            }
            return false;
        }
    }

    public static void main(String[] args) {
        int V = 6; // Number of vertices
        Graph graph = new Graph(V);
        graph.addEdge(0, 1);
        graph.addEdge(0, 2);
        graph.addEdge(1, 3);
        graph.addEdge(2, 3);
        graph.addEdge(3, 4);
        graph.addEdge(4, 5);

        List<Integer> liveNodes = graph.findLiveNodes();
        List<Integer> eNodes = graph.findENodes();
        List<Integer> deadNodes = graph.findDeadNodes();

        System.out.println("Live Nodes: " + liveNodes);
        System.out.println("E-Nodes: " + eNodes);
        System.out.println("Dead Nodes: " + deadNodes);
    }
}
