import java.util.*;

class Q1 {

    static class Graph {
        int V;
        List<List<Edge>> adjList;

        Graph(int V) {
            this.V = V;
            adjList = new ArrayList<>(V);
            for (int i = 0; i < V; i++) {
                adjList.add(new ArrayList<>());
            }
        }

        static class Edge implements Comparable<Edge> {
            int dest;
            int weight;

            Edge(int dest, int weight) {
                this.dest = dest;
                this.weight = weight;
            }

            @Override
            public int compareTo(Edge other) {
                return this.weight - other.weight;
            }
        }

        void addEdge(int src, int dest, int weight) {
            adjList.get(src).add(new Edge(dest, weight));
            adjList.get(dest).add(new Edge(src, weight));
        }

        void primMST() {
            boolean[] visited = new boolean[V];
            int[] parent = new int[V];
            int[] key = new int[V];
            Arrays.fill(key, Integer.MAX_VALUE);

            PriorityQueue<Edge> pq = new PriorityQueue<>();
            pq.offer(new Edge(0, 0));
            key[0] = 0;
            parent[0] = -1;

            while (!pq.isEmpty()) {
                int u = pq.poll().dest;
                visited[u] = true;

                for (Edge edge : adjList.get(u)) {
                    int v = edge.dest;
                    int weight = edge.weight;
                    if (!visited[v] && weight < key[v]) {
                        parent[v] = u;
                        key[v] = weight;
                        pq.offer(new Edge(v, key[v]));
                    }
                }
            }

            printMST(parent);
        }

        void printMST(int[] parent) {
            System.out.println("Edge   Weight");
            for (int i = 1; i < V; i++) {
                System.out.println(parent[i] + " - " + i + "    " + adjList.get(i).get(parent[i]).weight);
            }
        }
    }

    public static void main(String[] args) {
        int V = 5; // Number of vertices
        Graph graph = new Graph(V);
        graph.addEdge(0, 1, 2);
        graph.addEdge(0, 2, 3);
        graph.addEdge(0, 3, 6);
        graph.addEdge(1, 2, 5);
        graph.addEdge(1, 4, 1);
        graph.addEdge(2, 3, 4);
        graph.addEdge(2, 4, 3);
        graph.addEdge(3, 4, 7);

        System.out.println("Minimum Spanning Tree using Prim's Algorithm:");
        graph.primMST();
    }
}
