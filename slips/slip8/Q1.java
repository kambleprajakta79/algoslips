import java.util.*;

class Q1 {

    static class Item {
        int weight;
        int value;
        double ratio;

        Item(int weight, int value) {
            this.weight = weight;
            this.value = value;
            this.ratio = (double) value / weight;
        }
    }

    static double fractionalKnapsack(int[] weights, int[] values, int capacity) {
        int n = weights.length;

        List<Item> items = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            items.add(new Item(weights[i], values[i]));
        }

        // Sort items by decreasing ratio of value to weight
        Collections.sort(items, Comparator.comparingDouble((Item item) -> item.ratio).reversed());

        double totalValue = 0;
        int currentWeight = 0;

        for (Item item : items) {
            if (currentWeight + item.weight <= capacity) {
                // Take the whole item
                totalValue += item.value;
                currentWeight += item.weight;
            } else {
                // Take a fraction of the item
                int remainingCapacity = capacity - currentWeight;
                totalValue += (double) remainingCapacity * item.ratio;
                break;
            }
        }

        return totalValue;
    }

    public static void main(String[] args) {
        int[] weights = {10, 20, 30};
        int[] values = {60, 100, 120};
        int capacity = 50;

        double maxValue = fractionalKnapsack(weights, values, capacity);
        System.out.println("Maximum value that can be obtained: " + maxValue);
    }
}
