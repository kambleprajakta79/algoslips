import java.util.*;

public class Q2 {

    static class Graph {
        private int V;
        private int[][] graph;

        Graph(int V) {
            this.V = V;
            graph = new int[V][V];
        }

        void addEdge(int src, int dest, int weight) {
            graph[src][dest] = weight;
            graph[dest][src] = weight; // Undirected graph
        }

        List<Integer> nearestNeighbor(int start) {
            List<Integer> tour = new ArrayList<>();
            boolean[] visited = new boolean[V];

            tour.add(start);
            visited[start] = true;

            for (int i = 0; i < V - 1; i++) {
                int current = tour.get(tour.size() - 1);
                int next = -1;
                int minDistance = Integer.MAX_VALUE;

                for (int j = 0; j < V; j++) {
                    if (!visited[j] && graph[current][j] < minDistance) {
                        minDistance = graph[current][j];
                        next = j;
                    }
                }

                tour.add(next);
                visited[next] = true;
            }

            // Return to the starting point to complete the tour
            tour.add(start);
            return tour;
        }
    }

    public static void main(String[] args) {
        int V = 4; // Number of vertices
        Graph graph = new Graph(V);
        graph.addEdge(0, 1, 10);
        graph.addEdge(0, 2, 15);
        graph.addEdge(0, 3, 20);
        graph.addEdge(1, 2, 35);
        graph.addEdge(1, 3, 25);
        graph.addEdge(2, 3, 30);

        int start = 0; // Starting vertex
        List<Integer> tour = graph.nearestNeighbor(start);

        System.out.println("Tour using Nearest Neighbor Algorithm:");
        for (int vertex : tour) {
            System.out.print(vertex + " ");
        }
    }
}
