public class Q2 {

    static void sumOfSubset(int[] arr, int target) {
        boolean[] solution = new boolean[arr.length];
        backtrack(arr, 0, 0, target, solution);
    }

    static void backtrack(int[] arr, int index, int currentSum, int target, boolean[] solution) {
        if (currentSum == target) {
            System.out.print("Subset with the sum " + target + ": ");
            for (int i = 0; i < arr.length; i++) {
                if (solution[i]) {
                    System.out.print(arr[i] + " ");
                }
            }
            System.out.println();
        } else if (index < arr.length) {
            if (currentSum + arr[index] <= target) {
                solution[index] = true;
                backtrack(arr, index + 1, currentSum + arr[index], target, solution);
            }

            solution[index] = false;
            backtrack(arr, index + 1, currentSum, target, solution);
        }
    }

    public static void main(String[] args) {
        int[] arr = {10, 7, 5, 18, 12, 20, 15};
        int target = 35;

        sumOfSubset(arr, target);
    }
}
