public class Q2 {

    static int optimalBST(int[] keys, int[] freq) {
        int n = keys.length;
        int[][] cost = new int[n + 1][n + 1];

        // Initialize cost for single keys
        for (int i = 0; i < n; i++) {
            cost[i][i] = freq[i];
        }

        // Build BSTs for keys of different lengths
        for (int length = 2; length <= n; length++) {
            for (int i = 0; i <= n - length + 1; i++) {
                int j = i + length - 1;
                cost[i][j] = Integer.MAX_VALUE;

                // Try making each key in the range the root
                for (int r = i; r <= j; r++) {
                    int c = (r > i ? cost[i][r - 1] : 0) +
                            (r < j ? cost[r + 1][j] : 0) +
                            sum(freq, i, j);
                    if (c < cost[i][j]) {
                        cost[i][j] = c;
                    }
                }
            }
        }

        return cost[0][n - 1];
    }

    static int sum(int[] freq, int i, int j) {
        int total = 0;
        for (int k = i; k < j; k++) {
            total += freq[k];
        }
        return total;
    }

    public static void main(String[] args) {
        int[] keys = {10, 12, 20};
        int[] freq = {34, 8, 50};

        int minCost = optimalBST(keys, freq);
        System.out.println("Minimum cost of optimal BST: " + minCost);
    }
}
