import java.util.*;

public class Q1 {

    static class Graph {
        private int V;
        private List<List<Node>> adjList;

        Graph(int V) {
            this.V = V;
            adjList = new ArrayList<>(V);
            for (int i = 0; i < V; i++) {
                adjList.add(new ArrayList<>());
            }
        }

        static class Node implements Comparable<Node> {
            int dest;
            int weight;

            Node(int dest, int weight) {
                this.dest = dest;
                this.weight = weight;
            }

            @Override
            public int compareTo(Node other) {
                return this.weight - other.weight;
            }
        }

        void addEdge(int src, int dest, int weight) {
            adjList.get(src).add(new Node(dest, weight));
        }

        void dijkstra(int src) {
            int[] dist = new int[V];
            Arrays.fill(dist, Integer.MAX_VALUE);
            dist[src] = 0;

            PriorityQueue<Node> pq = new PriorityQueue<>();
            pq.offer(new Node(src, 0));

            while (!pq.isEmpty()) {
                int u = pq.poll().dest;

                for (Node neighbor : adjList.get(u)) {
                    int v = neighbor.dest;
                    int weight = neighbor.weight;
                    if (dist[u] != Integer.MAX_VALUE && dist[u] + weight < dist[v]) {
                        dist[v] = dist[u] + weight;
                        pq.offer(new Node(v, dist[v]));
                    }
                }
            }

            printDistances(dist);
        }

        void printDistances(int[] dist) {
            System.out.println("Vertex \t Distance from Source");
            for (int i = 0; i < V; i++) {
                System.out.println(i + " \t\t " + dist[i]);
            }
        }
    }

    public static void main(String[] args) {
        int V = 6; // Number of vertices
        Graph graph = new Graph(V);
        graph.addEdge(0, 1, 5);
        graph.addEdge(0, 2, 3);
        graph.addEdge(1, 3, 6);
        graph.addEdge(1, 2, 2);
        graph.addEdge(2, 4, 4);
        graph.addEdge(2, 5, 2);
        graph.addEdge(2, 3, 7);
        graph.addEdge(3, 4, 1);
        graph.addEdge(4, 5, 1);

        System.out.println("Shortest distances from source vertex:");
        graph.dijkstra(0);
    }
}
