public class Q2 {

    public static int[][] strassenMatrixMultiply(int[][] A, int[][] B) {
        int n = A.length;
        int[][] C = new int[n][n];

        if (n == 1) {
            C[0][0] = A[0][0] * B[0][0];
        } else {
            // Divide matrices into submatrices
            int[][] A11 = new int[n / 2][n / 2];
            int[][] A12 = new int[n / 2][n / 2];
            int[][] A21 = new int[n / 2][n / 2];
            int[][] A22 = new int[n / 2][n / 2];

            int[][] B11 = new int[n / 2][n / 2];
            int[][] B12 = new int[n / 2][n / 2];
            int[][] B21 = new int[n / 2][n / 2];
            int[][] B22 = new int[n / 2][n / 2];

            // Splitting input matrices into submatrices
            splitMatrix(A, A11, 0, 0);
            splitMatrix(A, A12, 0, n / 2);
            splitMatrix(A, A21, n / 2, 0);
            splitMatrix(A, A22, n / 2, n / 2);

            splitMatrix(B, B11, 0, 0);
            splitMatrix(B, B12, 0, n / 2);
            splitMatrix(B, B21, n / 2, 0);
            splitMatrix(B, B22, n / 2, n / 2);

            // Strassen's recursive calculations
            int[][] P1 = strassenMatrixMultiply(addMatrices(A11, A22), addMatrices(B11, B22));
            int[][] P2 = strassenMatrixMultiply(addMatrices(A21, A22), B11);
            int[][] P3 = strassenMatrixMultiply(A11, subtractMatrices(B12, B22));
            int[][] P4 = strassenMatrixMultiply(A22, subtractMatrices(B21, B11));
            int[][] P5 = strassenMatrixMultiply(addMatrices(A11, A12), B22);
            int[][] P6 = strassenMatrixMultiply(subtractMatrices(A21, A11), addMatrices(B11, B12));
            int[][] P7 = strassenMatrixMultiply(subtractMatrices(A12, A22), addMatrices(B21, B22));

            // Calculating result submatrices
            int[][] C11 = addMatrices(subtractMatrices(addMatrices(P1, P4), P5), P7);
            int[][] C12 = addMatrices(P3, P5);
            int[][] C21 = addMatrices(P2, P4);
            int[][] C22 = addMatrices(subtractMatrices(subtractMatrices(P1, P2), P3), P6);

            // Combining result submatrices into result matrix
            joinMatrices(C11, C, 0, 0);
            joinMatrices(C12, C, 0, n / 2);
            joinMatrices(C21, C, n / 2, 0);
            joinMatrices(C22, C, n / 2, n / 2);
        }

        return C;
    }

    public static void splitMatrix(int[][] input, int[][] output, int startRow, int startCol) {
        for (int i1 = 0, i2 = startRow; i1 < output.length; i1++, i2++) {
            for (int j1 = 0, j2 = startCol; j1 < output.length; j1++, j2++) {
                output[i1][j1] = input[i2][j2];
            }
        }
    }

    public static void joinMatrices(int[][] input, int[][] output, int startRow, int startCol) {
        for (int i1 = 0, i2 = startRow; i1 < input.length; i1++, i2++) {
            for (int j1 = 0, j2 = startCol; j1 < input.length; j1++, j2++) {
                output[i2][j2] = input[i1][j1];
            }
        }
    }

    public static int[][] addMatrices(int[][] A, int[][] B) {
        int n = A.length;
        int[][] C = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                C[i][j] = A[i][j] + B[i][j];
            }
        }
        return C;
    }

    public static int[][] subtractMatrices(int[][] A, int[][] B) {
        int n = A.length;
        int[][] C = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                C[i][j] = A[i][j] - B[i][j];
            }
        }
        return C;
    }

    public static void main(String[] args) {
        int[][] A = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 }, { 13, 14, 15, 16 } };
        int[][] B = { { 17, 18, 19, 20 }, { 21, 22, 23, 24 }, { 25, 26, 27, 28 }, { 29, 30, 31, 32 } };

        int[][] C = strassenMatrixMultiply(A, B);

        // Print the result
        for (int[] row : C) {
            for (int num : row) {
                System.out.print(num + " ");
            }
            System.out.println();
        }
    }
}
